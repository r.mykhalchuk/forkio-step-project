'use strict'

const gulp = require('gulp');
const prefixer = require('gulp-autoprefixer'); //lib for autoprefix
const concat = require('gulp-concat'); // for file concatination
const uglify = require('gulp-uglifyjs'); // js compression 
const sass = require('gulp-sass'); //scss --> css
const sourcemaps = require('gulp-sourcemaps'); //for debug
const rigger = require('gulp-rigger'); //to include one file into another via template
const imagemin = require('gulp-imagemin'); // images compression
const pngguant = require('imagemin-pngquant'); //addon for imagemin, working with png
const rimraf = require('rimraf'); 
const browserSync = require('browser-sync');//livereload and local server
const reload = browserSync.reload;
const cssnano = require('gulp-cssnano'); //css minification
//const rename = require('gulp-rename');
//const pug = require('gulp-pug');

let path = {               //file destinations
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/main.js',
        style: 'src/style/*.scss',
        img: 'src/img/**/*.*', 
        fonts: 'src/fonts/**/*.*',
        
    },

    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        libs:'bower_components/**/*.js',
        style: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        libs_css:'bower_components/**/*.css'
    },
    clean: './build'
};

let config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    open: 'local'
};

gulp.task('html-build', function () { //task for html-building
    return gulp.src(path.src.html) //source
        .pipe(rigger()) 
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({
            stream: true
        }));
})


gulp.task('js-build', function () {
    return gulp.src(path.src.js) 
        .pipe(rigger()) 
        .pipe(sourcemaps.init()) //initialize sourcemap
        .pipe(uglify()) //js file compression
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest(path.build.js)) //destination-build
        .pipe(reload({stream: true})); //server reload
});

gulp.task('libs-js', function () { //js libs 
    return gulp.src([
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/magnific-popup/dist/jquery.magnific-popup.min.js'
        ]) 
        .pipe(sourcemaps.init()) 
        .pipe(concat('libs.min.js'))//files concat
        .pipe(uglify())//compression
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest(path.build.js)) 
      });


// gulp.task('pug', function () {
//     return gulp.src('src/pug/*.pug')
//         .pipe(pug({ pretty: true }))
//         .pipe(gulp.dest('build/'))
//         .pipe(reload({ stream: true }))
// })

gulp.task('style-build', function () {
        return gulp.src(path.src.style) 
            .pipe(sourcemaps.init()) 
            .pipe(sass()) 
            .pipe(prefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'])) 
            .pipe(cssnano()) 
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(path.build.css))
            .pipe(reload({stream: true}));
    });
    
gulp.task('libs-css', function () {
   return gulp.src('bower_components/**/*.css')
        .pipe(concat('libs.min.css'))
        .pipe(cssnano()) //compress css
        .pipe(gulp.dest(path.build.css))
})

gulp.task('image-build', function () {
    return gulp.src(path.src.img)
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngguant()]
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({
            stream: true
        }))
});

gulp.task('fonts-build', function () {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) { //to remove build folder
    rimraf(path.clean, cb);
})

gulp.task('watch', function () {
    // gulp.watch('src/pug/*.pug',gulp.parallel('pug'));
    gulp.watch(path.watch.html, gulp.parallel('html-build'));
    gulp.watch(path.watch.style, gulp.parallel('style-build'));
    gulp.watch(path.watch.libs_css, gulp.parallel('libs-css'));
    gulp.watch(path.watch.libs, gulp.parallel('libs-js'));
    gulp.watch(path.watch.js, gulp.parallel('js-build'));
    gulp.watch(path.watch.img, gulp.parallel('image-build'));
    gulp.watch(path.watch.fonts, gulp.parallel('fonts-build'));
});

gulp.task('build',gulp.parallel('clean','image-build','libs-js','libs-css','fonts-build','style-build','js-build',
'html-build'));

gulp.task('default',gulp.parallel('clean','image-build','libs-js','libs-css','fonts-build','style-build','js-build',
'html-build','webserver','watch'));